﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPTask1
{
    class Matrix
    {
        private double[,] values;
        private readonly int size;
        public int Size { get { return size; } }

        public Matrix(int n, bool autofill)
        {
            size = n;
            values = new double[n, n];
            if (autofill)
            {
                Random rand = new Random();
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        values[i, j] = rand.NextDouble()*100;
                    }
                }
            }
        }
        public double this[int x, int y]
        {
            get
            {
                return values[x, y];
            }
            set
            {
                values[x, y] = value;
            }
        }

        public void Print()
        {
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                    Console.Write("{0:0.00}\t", values[i, j]);
                Console.WriteLine();
            }
        }

        public double Max(out int x, out int y)
        {
            double max = values[0, 0];
            int x1 = 0;
            int y1 = 0;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (values[i, j] > max)
                    {
                        max = values[i, j];
                        x1 = i;
                        y1 = j;
                    }
                }
            }
            x = x1;
            y = y1;
            return max;
        }

        public double[] Multiply(Vector v)
        {
            double[] result = new double[size];
            for (int i = 0; i < size; i++)
            {
                double sum = 0;
                for (int j = 0; j < size; j++)
                    sum += values[i, j] * v[j];
                result[i] = sum;
            }
            return result;
        }

        public Matrix Multiply(Matrix m)
        {
            Matrix result = new Matrix(size, false);
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    result[i, j] = 0;
                    for (int l = 0; l < size; l++)
                    {
                        result[i, j] += values[i, l] * m[l, j];
                    }
                }
            }
            return result;
        }

        //Алгоритм Штрассена (умножение матриц)
        public Matrix Strassen(Matrix m)
        {
            if (size <= 64)
                return Multiply(m);
            var a = Split();
            var b = m.Split();
            var p1 = (Add(a[0], a[3])).Strassen(Add(b[0], b[3]));
            var p2 = (Add(a[2], a[3])).Strassen(b[0]);
            var p3 = a[0].Strassen(Substract(b[1], b[3]));
            var p4 = a[3].Strassen(Substract(b[2], b[0]));
            var p5 = (Add(a[0], a[1])).Strassen(b[3]);
            var p6 = (Substract(a[2], a[0])).Strassen(Add(b[0], b[1]));
            var p7 = (Substract(a[1], a[3])).Strassen(Add(b[2], b[3]));
            var c11 = Add(Substract(Add(p1, p4), p5), p7);
            var c12 = Add(p3, p5);
            var c21 = Add(p2, p4);
            var c22 = Add(Add(Substract(p1, p2), p3), p6);

            return Join(c11, c12, c21, c22);
        }

        //Используется в алгоритме Штрассена
        private Matrix Join(Matrix c11, Matrix c12, Matrix c21, Matrix c22)
        {
            Matrix res = new Matrix(size, false);
            int cropedSize = size / 2;
            for (int i = 0; i < cropedSize; i++)
            {
                for (int j = 0; j < cropedSize; j++)
                {
                    res[i, j] = c11[i, j];
                    res[i, j + cropedSize] = c12[i, j];
                    res[i + cropedSize, j] = c21[i, j];
                    res[i + cropedSize, j + cropedSize] = c22[i, j];
                }
            }
            return res;
        }

        //Используется в алгоритме Штрассена
        private Matrix[] Split()
        {
            int half = size / 2;
            Matrix m1 = new Matrix(half, false);
            Matrix m2 = new Matrix(half, false);
            Matrix m3 = new Matrix(half, false);
            Matrix m4 = new Matrix(half, false);

            for (int i = 0; i < half; i++)
            {
                for (int j = 0; j < half; j++)
                {
                    m1[i, j] = values[i, j];
                    m2[i, j] = values[i, j + half];
                    m3[i, j] = values[i + half, j];
                    m4[i, j] = values[i + half, j + half];
                }
            }
            return new[] { m1, m2, m3, m4 };
        }

        //Используется в алгоритме Штрассена
        public static Matrix Add(Matrix m1, Matrix m2)
        {
            Matrix result = new Matrix(m1.size, false);
            for (int i = 0; i < m1.size; i++)
                for (int j = 0; j < m1.size; j++)
                    result[i, j] = m1[i, j] + m2[i, j];
            return result;
        }

        //Используется в алгоритме Штрассена
        public static Matrix Substract(Matrix m1, Matrix m2)
        {
            Matrix result = new Matrix(m1.size, false);
            for (int i = 0; i < m1.size; i++)
                for (int j = 0; j < m1.size; j++)
                    result[i, j] = m1[i, j] - m2[i, j];
            return result;
        }

        //Используется в алгоритме Штрассена
        public static bool Compare(Matrix m1, Matrix m2)
        {
            if (m1.Size != m2.Size || m1 == null || m2 == null) return false;
            for (int i = 0; i < m1.Size; i++)
            {
                for (int j = 0; j < m1.Size; j++)
                {
                    if (m1[i, j] != m2[i, j]) return false;
                }
            }
            return true;
        }
    }
}

