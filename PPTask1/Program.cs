﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace PPTask1
{
    class Program
    {
        static void MaxValue()
        {
            Stopwatch clock = new Stopwatch();

            for (int i = 64; i <= 2048; i *= 2)
            {
                int x = 0, y = 0;
                double max = 0.0f;
                Matrix m = new Matrix(i, true);
                Console.WriteLine("Matrix ({0})", i);

                clock.Start();
                max = m.Max(out x, out y);
                clock.Stop();
                var c = clock.Elapsed;
                Console.WriteLine("Execution time: {0} milliseconds.\n{1} min, {2} sec, {3} ms",
                c.TotalMilliseconds, c.Minutes, c.Seconds, c.Milliseconds / 10);
                Console.WriteLine();
                clock.Reset();
            }
        }

        static void MultiplyVector()
        {
            Stopwatch clock = new Stopwatch();

            for (int i = 64; i <= 2048; i *= 2)
            {
                double[] r = new double[i];
                Matrix m = new Matrix(i, true);
                Vector v = new Vector(i, true);
                Console.WriteLine("({0})-sized Matrix and Vector Created", i);

                clock.Start();
                r = m.Multiply(v);
                clock.Stop();
                var t = clock.Elapsed;
                Console.WriteLine("Execution time: {0} milliseconds.\n{1} min, {2} sec, {3} ms",
                t.TotalMilliseconds, t.Minutes, t.Seconds, t.Milliseconds / 10);
                Console.WriteLine();
                clock.Reset();
            }
        }

        static void MultiplyMatrix()
        {
            Stopwatch clock = new Stopwatch();

            for (int i = 64; i <= 2048; i *= 2)
            {
                Matrix m1 = new Matrix(i, true);
                Matrix m2 = new Matrix(i, true);
                Matrix r = new Matrix(i, false);

                Console.WriteLine("({0})-sized Matrixes Created", i);
                clock.Start();
                r = m1.Multiply(m2);
                clock.Stop();
                var c = clock.Elapsed;
                Console.WriteLine("Execution time: {0} milliseconds.\n{1} min, {2} sec, {3} ms", 
                c.TotalMilliseconds, c.Minutes, c.Seconds, c.Milliseconds / 10);
                Console.WriteLine();
                clock.Reset();
            }
        }

        static void StrassenAlg()
        {
            Stopwatch clock = new Stopwatch();

            for (int i = 64; i <= 2048; i *= 2)
            {
                Matrix m1 = new Matrix(i, true);
                Matrix m2 = new Matrix(i, true);
                Matrix r = new Matrix(i, false);
                Console.WriteLine("({0})-sized Matrixes Created", i);

                clock.Start();
                r = m1.Strassen(m2);
                clock.Stop();
                var t = clock.Elapsed;
                Console.WriteLine("Execution time: {0} milliseconds.\n{1} min, {2} sec, {3} ms",
                t.TotalMilliseconds, t.Minutes, t.Seconds, t.Milliseconds / 10);
                Console.WriteLine();
                clock.Reset();
            }
        }

        static void CheckResult()
        {
            Matrix m1 = new Matrix(8, true);
            Matrix m2 = new Matrix(8, true);
            Matrix r1 = m1.Multiply(m2);
            Matrix r2 = m1.Strassen(m2);

            if (Matrix.Compare(r1, r2))
            {
                Console.WriteLine("Multiply results match");
                Console.WriteLine();
            }
            else
                Console.WriteLine("One or both multiply operations were incorrect!");
        }

        static void Main(string[] args)
        {
            //Matrix m1 = new Matrix(8, true);
            //m1.Print();
            CheckResult();
            MaxValue();
            MultiplyVector();
            MultiplyMatrix();
            StrassenAlg();
            
        }
    }
}
