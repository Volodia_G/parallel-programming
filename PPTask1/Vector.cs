﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPTask1
{
    class Vector
    {
        private double[] values;
        private readonly int size;
        public int Size { get { return size; } }

        public Vector(int n, bool autofill)
        {
            if (autofill)
            {
                size = n;
                values = new double[n];
                Random rand = new Random();
                for (int i = 0; i < n; i++)
                    values[i] = rand.NextDouble() * 100;
            }
        }

        public double this[int n]
        {
            get
            {
                return values[n];
            }
        }

        public void Print()
        {
            for (int i = 0; i < size; i++)
                Console.Write("{0:0.00}\t", values[i]);
            Console.WriteLine();
        }
    }
}
